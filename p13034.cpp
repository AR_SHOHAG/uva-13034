#include <cstdio>
#include <iostream>
using namespace std;

int main()
{
    int s, arr[13], i, ck=0;
    scanf("%d", &s);

    for(int j=1; j<=s; ++j){
        for(i=0; i<13;++i){
            scanf("%d", &arr[i]);
            if(arr[i]==0)
                ck++;
        }
        if(ck>0){
            printf("Set #%d: No\n", j);
        }
        else
            printf("Set #%d: Yes\n", j);
        ck=0;

    }

    return 0;
}
